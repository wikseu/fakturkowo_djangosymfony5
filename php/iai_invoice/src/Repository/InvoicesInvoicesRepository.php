<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/Repository/InvoicesInvoicesRepository.php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Description of ItemsItemsRepository
 *
 * @author wiks
 */
class InvoicesInvoicesRepository extends EntityRepository
{
    
    /** sprawdź, czy ten numer nadawany fakturze nie jest już w użyciu
     * 
     * @param type $fNr
     * @param type $fid
     * @return type
     */
    public function isFNRused($fNr, $fid=null) {
        
        // $invoice_obj = Invoices.objects.filter(nr=fNr).exclude(id=fid).first()
        
        $ret = null;
        if($fid) {
            $ret = $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:InvoicesInvoices p '
                    . 'WHERE '
                    . 'p.nr = ?1 '
                    . 'AND p.id != ?2 '
            )
            ->setParameter(1, $fNr)
            ->setParameter(2, $fid)
            ->getResult();
        };
        return $ret;
    }
    
    /** usuń fakturę
     * 
     * @param type $invoice_id
     */
    public function deleteInvoiceAllItems($invoice_id) {
        
        $entity_products = $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:InvoicesInvoices p '
                    . 'WHERE '
                    . 'p.id = ?1 '
            )
            ->setParameter(1, $invoice_id)
            ->getResult();
        foreach ($entity_products as $product) {
            $this->getEntityManager()->remove($product);
        }
        $this->getEntityManager()->flush();
        return null;
    }

    
}

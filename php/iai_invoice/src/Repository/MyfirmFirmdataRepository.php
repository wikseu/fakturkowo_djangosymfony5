<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/Repository/MyfirmFirmdataRepository.php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Description of MyfirmFirmdataRepository
 *
 * @author wiks
 */
class MyfirmFirmdataRepository extends EntityRepository
{
    
    /** pobierz dane mojej firmy (jest w liście firm na pozycji ID=1)
     * 
     * @return type
     */
    public function getAllCustomers()
    {
        /* $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class); 
         * $repository->getAllCustomers()
         */
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:MyfirmFirmdata p '
                    . 'WHERE p.id > 1 '
                    . 'ORDER BY p.fname ASC'
            )
            ->getResult();
    }
    
    /** odszukaj firmy, których jedne z numerów identyfikacyjnych zawiera wpisywane cyfry
     * 
     * @param string $lookfor
     * @return type
     */
    public function lookforNipRegonPeselLike(string $lookfor)
    {
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p.id, p.fname FROM App:MyfirmFirmdata p '
                    . 'WHERE '
                    . '( '
                    . 'p.nip LIKE ?1 '
                    . 'OR p.regon LIKE ?1 '
                    . 'OR p.pesel LIKE ?1 '
                    . ') '
                    . 'AND p.id > 1 '
                    . 'ORDER BY p.fname ASC'
            )
            ->setParameter(1, '%'.$lookfor.'%')
            ->getResult();
    }
    
    /** 
     * 
     * @param string $lookfor
     * @return type
     *
    public function lookforNipLike(string $lookfor)
    {
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p.id FROM App:MyfirmFirmdata p '
                    . 'WHERE p.nip LIKE ?1 ' . ' AND p.id > 1'
                    . 'ORDER BY p.fname ASC'
            )
            ->setParameter(1, $lookfor)
            ->getResult();
    }*/

    /**
     * 
     * @param string $lookfor
     * @return type
     *
    public function lookforRegonLike(string $lookfor)
    {
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p.id FROM App:MyfirmFirmdata p '
                    . 'WHERE p.regon LIKE ?1 '
                    . 'ORDER BY p.fname ASC'
            )
            ->setParameter(1, $lookfor)
            ->getResult();
    }*/
    
    /**
     * 
     * @param string $lookfor
     * @return type
     *
    public function lookforPeselLike(string $lookfor)
    {
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p.id FROM App:MyfirmFirmdata p '
                    . 'WHERE p.pesel LIKE ?1 '
                    . 'ORDER BY p.fname ASC'
            )
            ->setParameter(1, $lookfor)
            ->getResult();
    }*/
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/Repository/InvoicesInvoicesitemsRepository.php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Description of MyfirmFirmdataRepository
 *
 * @author wiks
 */
class InvoicesInvoicesitemsRepository extends EntityRepository
{
    
    /** pobierz wszystkie pozycje z faktury
     * 
     * @param type $invoice
     * @return type
     */
    public function getAllItemsForInvoice($invoice)
    {
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:InvoicesInvoicesitems p '
                    . 'WHERE '
                    . 'p.invoice = ?1 '
            )
            ->setParameter(1, $invoice)
            ->getResult();
    }
    
    /** pobierz jeśli istnieje pozycję przedmiotu z faktury
     * 
     * @param type $invoice_id
     * @param type $item_id
     * @return type
     */
    public function getInvoiceItemIfExist($invoice_id, $item_id) {
        
        $res = null;
        $ret = $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:InvoicesInvoicesitems p '
                    . 'WHERE '
                    . 'p.invoice = ?1 '
                    . 'AND p.items = ?2 '
            )
            ->setParameter(1, $invoice_id)
            ->setParameter(2, $item_id)
            ->getResult();
        if($ret) {
            $res = $ret[0];
        }
        return $res;
    }
    
    /** usuń przedmiot z faktury
     * 
     * @param type $invoice_id
     * @param type $item_id
     * @param type $iid
     */
    public function deleteInvoicesItemsPosition($invoice_id, $item_id, $iid) {
        
        $entity_products = $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:InvoicesInvoicesitems p '
                    . 'WHERE '
                    . 'p.invoice = ?1 '
                    . 'AND p.items = ?2 '
                    . 'AND p.id = ?3 '
            )
            ->setParameter(1, $invoice_id)
            ->setParameter(2, $iid)
            ->setParameter(3, $item_id)
            ->getResult();
        foreach ($entity_products as $product) {
            $this->getEntityManager()->remove($product);
        }
        $this->getEntityManager()->flush();        
        return null;
    }
        
    /** usuń wszystkie przedmioty z faktury
     * 
     * @param type $invoice_id
     */
    public function deleteInvoiceAllItems($invoice_id) {
        
        $entity_products = $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:InvoicesInvoicesitems p '
                    . 'WHERE '
                    . 'p.invoice = ?1 '
            )
            ->setParameter(1, $invoice_id)
            ->getResult();
        foreach ($entity_products as $product) {
            $this->getEntityManager()->remove($product);
        }
        $this->getEntityManager()->flush();
        return null;
    }

}

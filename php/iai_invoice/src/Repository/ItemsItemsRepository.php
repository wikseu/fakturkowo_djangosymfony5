<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/Repository/ItemsItemsRepository.php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Description of ItemsItemsRepository
 *
 * @author wiks
 */
class ItemsItemsRepository extends EntityRepository
{
    
    /** odszukaj towary zawierający znaki w nazwie
     * 
     * @param string $lookfor
     * @return type
     */
    public function lookforItemLike(string $lookfor)
    {
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p.id, p.name FROM App:ItemsItems p '
                    . 'WHERE '
                    . 'p.name LIKE ?1 '
            )
            ->setParameter(1, '%'.$lookfor.'%')
            ->getResult();
    }
    
}

<?php
// src/Controller/MyFirmController.php 
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface; 
use App\Entity\InvoicesInvoices;
use App\Entity\ItemsItems;
use App\Entity\InvoicesInvoicesitems;
use App\Entity\MyfirmFirmdata;
use App\WikS\BreadcrumbsUtil;
use App\WikS\ValidationInvoiceUtils;
use App\WikS\InvoicesUtils;
use App\WikS\MyFirmUtils;
use Psr\Log\LoggerInterface;

/** wszystkie widoki związane z fakturami
 * 
 */
class InvoicesController extends AbstractController
{
    
    /** widok menu głównego
     * 
     * @Route("/i", name="invoices_main")
     */    
    public function invoices_main()
    {
        $bcu = new BreadcrumbsUtil();
        $contents = $this->renderView('invoices/invoices_main.html.twig', 
                                      ['breadcrumbs'=> $bcu->home()]);
        return new Response($contents);        
    }
        
    /** widok listy faktur
     * 
     * @Route("/i/l", name="invoices_list")
     */    
    public function invoices_list(Request $request, PaginatorInterface $paginator, LoggerInterface $logger)
    {
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('index');
            }
        }        
        $bcu = new BreadcrumbsUtil();
        $repository = $this->getDoctrine()->getRepository(InvoicesInvoices::class); 
        $contents = $this->renderView('invoices/invoices_list.html.twig', 
                ['invoices_objs' => $paginator->paginate($repository->findAll(),
                                                         $request->query->getInt('page', 1), 10),
                 'breadcrumbs'=> $bcu->invoices()]);
        return new Response($contents);
    }
    
    /** widok dodanie nowej faktury - wybór klienta
     * 
     * @Route("/i/a/{customer_id}", name="invoice_add")
     */    
    public function invoice_add(Request $request, LoggerInterface $logger, $customer_id=null)
    {
        
        $logger->debug('widok dodanie nowej faktury - wybór klienta '.$customer_id);
        $bcu = new BreadcrumbsUtil();
        $icom = new InvoicesUtils();
        
        $customer = null;
        if ($customer_id && $customer_id > 1) {
            $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class); 
            $customer = $repository->find($customer_id);
        }
        if ($customer){
            $logger->debug('znaleziono klienta: '.$customer->getFname());
            $em = $this->getDoctrine()->getManager();
            $invoice = $icom->create_empty_invoice($em, $customer);
            return $this->redirectToRoute('invoice_edit', ['invoice_id' => $invoice->getId()]);
        }
        $contents = $this->renderView('invoices/invoice_add.html.twig', 
                ['breadcrumbs'=> $bcu->invoice_one('faktura')]);
        return new Response($contents);
    }
    
    /** widok pokazania jednej faktury
     * 
     * @Route("/i/s/{invoice_id}", name="invoice_show")
     */
    public function invoice_show(Request $request, LoggerInterface $logger, ?int $invoice_id) {
        
        $logger->debug('widok edycjaj faktury '. $invoice_id);
        $bcu = new BreadcrumbsUtil();
        $icom = new InvoicesUtils();
        $invoice_obj = null;
        if($invoice_id) {
            $repository0 = $this->getDoctrine()->getRepository(InvoicesInvoices::class); 
            $invoice_obj = $repository0->find($invoice_id);
        }
        if(!$invoice_obj){
            return $this->redirectToRoute('invoices_list');
        }

        $em = $this->getDoctrine()->getManager();
        $web_context = [];
        $web_context['invoice'] = $invoice_obj;
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoices_list');
            }
            if($action == 'edit') {
                return $this->redirectToRoute('invoice_edit', ['invoice_id' => $invoice_id]);
            }
            if($action == 'delete') {
                return $this->redirectToRoute('invoice_delete', ['invoice_id' => $invoice_id]);
            }
            if($action == 'OK') {
                
                // todo drukuj do PDF
                
            }
        }
        $web_context = $icom->common_invoice_middle_show($em, $web_context, $invoice_obj);
        $web_context['breadcrumbs'] = $bcu->invoice_one('faktura '.$web_context['fNr']);
        $contents = $this->renderView('invoices/invoice_show.html.twig', $web_context);
        return new Response($contents);
    }
    
    /** widok edycji faktury
     * 
     * @Route("/i/e/{invoice_id}", name="invoice_edit")
     */    
    public function invoice_edit(Request $request, LoggerInterface $logger, ?int $invoice_id)
    {

        $logger->debug('widok edycjaj faktury '. $invoice_id);
        $bcu = new BreadcrumbsUtil();
        $icom = new InvoicesUtils();
        $viu = new ValidationInvoiceUtils();
        $invoice_obj = null;
        if($invoice_id) {
            //$invoice_obj = Invoices.objects.filter(id=invoice_id).first()
            $repository0 = $this->getDoctrine()->getRepository(InvoicesInvoices::class); 
            $invoice_obj = $repository0->find($invoice_id);
        }
        if(!$invoice_obj){
            return $this->redirectToRoute('invoices_list');
        }
        $em = $this->getDoctrine()->getManager();
        $web_context['invoice'] = $invoice_obj;
        list($web_context['fNr'],  
             $web_context['customer'], 
             $seller, 
             $web_context['invoices_items_richer'], 
             $web_context['vat_sum'], 
             $web_context['price_sum_netto'], 
             $web_context['price_sum_brutto'], 
             $web_context['paid_form'], 
             $web_context['person_auth_name'], 
             $web_context['dt_created'], 
             $web_context['dt_pait_to'], 
             $web_context['dt_delivery'], 
             $web_context['paid_sum_brutto']) = $icom->invoice_pickup($em, $invoice_obj);
        $web_context['seller'] = $seller;
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') { 
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoices_list');
            }
            if($action == 'delete') { 
                $logger->debug('delete');
                return $this->redirectToRoute('invoice_delete', ['invoice_id' => $invoice_id]);
            }
            if($action == 'OK') {
                $logger->debug('zapis zmian - wyjście do widoku drukowanlego faktury');
                $action = $request->request->get('action', null);

                list($fNr, 
                     $person_auth_name, 
                     $dt_created, 
                     $dt_pait_to, 
                     $dt_delivery, 
                     $paid_sum_brutto, 
                     $pay_form_id) = $icom->data_invoice_from_postrequest($request);
                $web_context = $icom->from_request_to_webcontext_invoice($web_context,
                                                                         $fNr,
                                                                         $invoice_obj->getCustomer(),
                                                                         $seller,
                                                                         $person_auth_name,
                                                                         $dt_created,
                                                                         $dt_pait_to,
                                                                         $dt_delivery,
                                                                         $paid_sum_brutto);
                list($fv_fNr, 
                     $fv_person_auth_name, 
                     $fv_dt_created, 
                     $fv_dt_pait_to, 
                     $fv_dt_delivery, 
                     $errors_message_list, 
                     $errors_message_redclass_list) 
                        = $viu->validate_complet_invoice_data($em,
                                                              $invoice_obj,
                                                              $fNr,
                                                              $person_auth_name,
                                                              $dt_created,
                                                              $dt_pait_to,
                                                              $dt_delivery
                                                              );
                if(!$errors_message_redclass_list){
                    $invoice_obj->setNr($fv_fNr);
                    if($fv_dt_created){
                        $invoice_obj->setDtCreated(new \DateTime($fv_dt_created));
                    }
                    if($fv_dt_delivery){
                        $invoice_obj->setdtDelivery(new \DateTime($fv_dt_delivery));
                    }
                    if($fv_dt_pait_to){
                        $invoice_obj->setDtPaitTo(new \DateTime($fv_dt_pait_to));
                    }
                    $invoice_obj->setPersonAuthName($fv_person_auth_name);
                    $invoice_obj->setPriceSumBrutto(0);
                    $invoice_obj->setPriceSumNetto(0);
                    $invoice_obj->setPaidSumBrutto($paid_sum_brutto);
                    $invoice_obj->setPayFormId($pay_form_id);  // null to przelew
                    $em->flush();
                    return $this->redirectToRoute('invoice_show', ['invoice_id' => $invoice_id]);
                }
                $web_context['errors_message_list'] = $errors_message_list;
                $web_context['errors_message_redclass_list'] = $errors_message_redclass_list;
            }
            if($action == 'delete') {
                $logger->debug('usunięcie faktury');
                
            }
            if($action == 'add_item') {
                $logger->debug('dodanie dowej pozycji do faktury');
                return $this->redirectToRoute('invoice_add_item_choice', ['invoice_id' => $invoice_id]);
            }
        }
        $web_context['breadcrumbs'] = $bcu->invoice_one('faktura '.$web_context['fNr']);
        $contents = $this->renderView('invoices/invoice_edit.html.twig', $web_context);
        return new Response($contents);
    }
     
    /** widok edycji faktury, dodania towaru - wyboru ilości tego towaru
     * 
     * @Route("/i/ai/{invoice_id}", name="invoice_add_item_choice")
     */    
    public function invoice_add_item_choice(Request $request, LoggerInterface $logger, ?int $invoice_id)
    {

        $logger->debug('widok edycjaj faktury '. $invoice_id);   
        $bcu = new BreadcrumbsUtil();
        $icom = new InvoicesUtils();
        $invoice_obj = null;
        if($invoice_id) {
            $repository0 = $this->getDoctrine()->getRepository(InvoicesInvoices::class); 
            $invoice_obj = $repository0->find($invoice_id);
        }
        if(!$invoice_obj){
            return $this->redirectToRoute('invoices_list');
        }
        // to będzie potrzebne przy dodawaniu towaru
        $web_context['invoice'] = $invoice_obj;
        $em = $this->getDoctrine()->getManager();
                
        list($web_context['fNr'],  
             $web_context['customer'], 
             $web_context['seller'], 
             $web_context['invoices_items_richer'], 
             $web_context['vat_sum'], 
             $web_context['price_sum_netto'], 
             $web_context['price_sum_brutto'], 
             $web_context['paid_form'], 
             $web_context['person_auth_name'], 
             $web_context['dt_created'], 
             $web_context['dt_pait_to'], 
             $web_context['dt_delivery'], 
             $web_context['paid_sum_brutto']) = $icom->invoice_pickup($em, $invoice_obj);

        $web_context['breadcrumbs'] = $bcu->invoice_one('faktura '.$web_context['fNr']); 
        $contents = $this->renderView('invoices/invoice_add_item_choice.html.twig', $web_context);
        return new Response($contents);
    }
    
    /** widok edycji faktury, dodania towaru - wyboru ilości tego towaru
     * 
     * @Route("/i/ai/{invoice_id}/{item_id}", name="invoice_add_item")
     */    
    public function invoice_add_item(Request $request, LoggerInterface $logger, ?int $invoice_id, ?int $item_id=null)
    {

        $logger->debug('widok edycjaj faktury '. $invoice_id);   
        $bcu = new BreadcrumbsUtil();
        $icom = new InvoicesUtils();
        $errors_message_list = [];
        $invoice_obj = null;
        if($invoice_id) {
            $repository0 = $this->getDoctrine()->getRepository(InvoicesInvoices::class); 
            $invoice_obj = $repository0->find($invoice_id);
        }
        if(!$invoice_obj){
            return $this->redirectToRoute('invoices_list');
        }
        $item_obj = null;
        if($item_id) {
            $repository1 = $this->getDoctrine()->getRepository(ItemsItems::class); 
            $item_obj = $repository1->find($item_id);
        }
        if(!$item_obj) {
            $web_context['breadcrumbs'] = $bcu->invoice_one('faktura');        
            $contents = $this->renderView('invoices/invoice_add_item_choice.html.twig', $web_context);
            return new Response($contents);
        }
        $web_context['item'] = $item_obj;
        $logger->debug('$item_obj = ' . $item_obj->getName());
        // to będzie potrzebne przy dodawaniu towaru
        $web_context['invoice'] = $invoice_obj;
        $em = $this->getDoctrine()->getManager();

        list($web_context['fNr'],  
             $web_context['customer'], 
             $web_context['seller'], 
             $web_context['invoices_items_richer'], 
             $web_context['vat_sum'], 
             $web_context['price_sum_netto'], 
             $web_context['price_sum_brutto'], 
             $web_context['paid_form'], 
             $web_context['person_auth_name'], 
             $web_context['dt_created'], 
             $web_context['dt_pait_to'], 
             $web_context['dt_delivery'], 
             $web_context['paid_sum_brutto']) = $icom->invoice_pickup($em, $invoice_obj);

        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoice_edit', ['invoice_id' => $invoice_id]);
            }
            if($action == 'OK') {
                $logger->debug('zapis zmian - wyjście do widoku drukowanlego faktury');
                $item_amount = $request->request->get('item_amount', null);
                list($invoices_item_obj, 
                     $errors_message_list) = $icom->add_item_to_invoice(
                             $em, 
                             $invoice_obj, 
                             $item_obj, 
                             $item_amount, 
                             $errors_message_list);
                if($invoices_item_obj) {
                    $logger->debug('dodany/updatowany przedmiot do faktury');
                    return $this->redirectToRoute('invoice_edit', ['invoice_id' => $invoice_id]);
                }
                $web_context['errors_message_list'] = $errors_message_list;
            }
        }
        $web_context['breadcrumbs'] = $bcu->invoice_one('faktura '.$web_context['fNr']);
        $contents = $this->renderView('invoices/invoice_edit_add_item.html.twig', $web_context);
        return new Response($contents);
    }

    /** usuwanie pozycji z faktury
     * 
     * @Route("/i/di/{invoice_id}/{item_id}/{iid}", name="invoice_del_item")
     */
    public function invoice_del_item(Request $request, LoggerInterface $logger, ?int $invoice_id, ?int $item_id, ?int $iid) {
        
        $logger->debug('DELETEUJE faktury '. $invoice_id);   
        $logger->debug('$item_id = ' . $item_id);
        $logger->debug('$iid = ' . $iid);
        
        $invoice_obj = null;
        if($invoice_id) {
            $repository0 = $this->getDoctrine()->getRepository(InvoicesInvoices::class); 
            $invoice_obj = $repository0->find($invoice_id);
        }
        if(!$invoice_obj){
            return $this->redirectToRoute('invoices_list');
        }
        if($item_id) {
            // invoice_id: ID faktury
            // item_id: ID pozycji fakturowej
            // iid: ID towaru na liście towarów
            $repository2 = $this->getDoctrine()->getRepository(InvoicesInvoicesitems::class);
            $repository2->deleteInvoicesItemsPosition($invoice_id, $item_id, $iid);
        }
        return $this->redirectToRoute('invoice_edit', ['invoice_id' => $invoice_id]);
    }

    /** usuwanie faktury
     * 
     * @Route("/i/d/{invoice_id}", name="invoice_delete")
     */
    public function invoice_delete(Request $request, LoggerInterface $logger, ?int $invoice_id) {
    
        $logger->debug('usuwanie faktury '. $invoice_id);   
        $bcu = new BreadcrumbsUtil();
        $icom = new InvoicesUtils();
//        $errors_message_list = [];
        $invoice_obj = null;
        if($invoice_id) {
            $repository0 = $this->getDoctrine()->getRepository(InvoicesInvoices::class); // InvoicesInvoicesitems
            $invoice_obj = $repository0->find($invoice_id);
        }
        if(!$invoice_obj){
            return $this->redirectToRoute('invoices_list');
        }
        $em = $this->getDoctrine()->getManager();
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoices_list');
            }
            if($action == 'delete') {
                $logger->debug('delete, skasuj wszystkie pozycje faktury a później fakturę');
                $repository1 = $this->getDoctrine()->getRepository(InvoicesInvoicesitems::class); 
                $repository1->deleteInvoiceAllItems($invoice_id);
                $em->remove($invoice_obj);
                $em->flush();
                return $this->redirectToRoute('invoices_list');
            }
        }
        $web_context = [];
        $web_context['invoice'] = $invoice_obj;
        $web_context = $icom->common_invoice_middle_show($em, $web_context, $invoice_obj);
        $web_context['breadcrumbs'] = $bcu->invoice_one('faktura '.$web_context['fNr']);
        $contents = $this->renderView('invoices/invoice_delete.html.twig', $web_context);
        return new Response($contents);
    }
    
}


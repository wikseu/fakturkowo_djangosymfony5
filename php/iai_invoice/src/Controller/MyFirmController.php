<?php
// src/Controller/MyFirmController.php 
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\MyfirmFirmdata;
use App\WikS\MyFirmUtils;
use App\WikS\BreadcrumbsUtil;
use App\WikS\ValidationFirm;
use Psr\Log\LoggerInterface;

/** wszystkie widoki własnej firmy oraz widok treści zadania IAI
 * 
 */
class MyFirmController extends AbstractController
{
    
    
    /** widok zadania
     * @Route("/", name="index")
     */    
    public function index()
    {
        
        $contents = $this->renderView('index.html.twig', []);
        return new Response($contents);
    }
    
    /**  zwraca widok z podglądem danych własnej firmy
     * @Route("/m", name="myfirm_main")
     */    
    public function myfirm_main(Request $request, LoggerInterface $logger)
    {
        
        $myfirm = new MyFirmUtils();
        $bcu = new BreadcrumbsUtil();        
        $web_context = [];
        $web_context['breadcrumbs']= $bcu->myfirm();
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoices_main');
            }
            if($action == 'create_edit') {
                $logger->debug('edycja');
                return $this->redirectToRoute('myfirm_edit');
            }
        }
        // pobieram dane własnej firmy (jest jako firma o ID = 1)
        $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class); 
        $firm_data_obj = $repository->find(1);
        $web_context['firm_data_obj'] = $firm_data_obj;
        $contents = $this->renderView('myfirm/myfirm_show.html.twig', 
                $myfirm->update_webkontext($web_context, $firm_data_obj)
                );
        return new Response($contents);
    }
    
    /**  zwraca widok z edycją danych własnej firmy
     * @Route("/m/e", name="myfirm_edit")
     */    
    public function myfirm_edit(Request $request, LoggerInterface $logger)
    {
        $myfirm = new MyFirmUtils();
        $bcu = new BreadcrumbsUtil();        
        $web_context = [];
        $web_context['breadcrumbs']= $bcu->myfirm();
    
        $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class); 
        $firm_data_obj = $repository->find(1);
        
        $em = $this->getDoctrine()->getManager();
        if(!$firm_data_obj) {
            // create empty myfirm
            $firm_data_obj = new MyfirmFirmdata();
            $firm_data_obj->setId(1);
            $em->persist($firm_data_obj);
            $em->flush();
        }
        
        $web_context['firm_data_obj'] = $firm_data_obj;
        $web_context = $myfirm->update_webkontext($web_context, $firm_data_obj); 
        
        $errors_message_list = [];
        $errors_message_redclass_list = [];
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('wyjście z edycji bez zapisu');
                return $this->redirectToRoute('myfirm_main');
            }
            if($action == 'OK') {
                $logger->debug('zatwierdzenie edytowanych danych, validacja i zapis');
                $vf = new ValidationFirm();
                list($firmName, 
                     $address_list, 
                     $mInputEmail, 
                     $firmNip, 
                     $firmRegon, 
                     $firmPesel, 
                     $firm_account_list) = $myfirm->data_firm_from_postrequest($request);
                
                //uzupełniam kontext, gdyby nie pyknęło...
                $web_context = $myfirm->from_request_to_webcontext_firm(
                                                    $web_context,
                                                    $firmName,
                                                    $address_list,
                                                    $mInputEmail,
                                                    $firmNip,
                                                    $firmRegon,
                                                    $firmPesel,
                                                    $firm_account_list);
                
                list($fname, 
                     $address_json, 
                     $email,
                     $nip, 
                     $regon, 
                     $pesel, 
                     $bank_json, 
                     $errors_message_list, 
                     $errors_message_redclass_list) = $vf->validate_complet_firm_data(
                                            $firmName,
                                            $address_list[0], $address_list[1], $address_list[2], $address_list[3], $address_list[4],
                                            $mInputEmail,
                                            $firmNip,
                                            $firmRegon,
                                            $firmPesel,
                                            $firm_account_list[0], $firm_account_list[1]);
                
                if(!$errors_message_redclass_list) {
                    $firm_data_obj->setId(1);
                    $firm_data_obj->setFname($fname);
                    $firm_data_obj->setAddressJson($address_json);
                    $firm_data_obj->setEmail($email);
                    $firm_data_obj->setNip($nip);
                    $firm_data_obj->setRegon($regon);
                    $firm_data_obj->setPesel($pesel);
                    $firm_data_obj->setBankAccount($bank_json);
                    $em->persist($firm_data_obj);
                    $em->flush();
                    return $this->redirectToRoute('myfirm_main');
                }
            }
        }
        $web_context['errors_message_list'] = $errors_message_list;
        $web_context['errors_message_redclass_list'] = $errors_message_redclass_list;

        $contents = $this->renderView('myfirm/myfirm_edit.html.twig', $web_context);
        return new Response($contents);
    }    
    
}

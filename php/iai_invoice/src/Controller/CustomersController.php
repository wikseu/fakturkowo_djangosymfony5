<?php
// src/Controller/MyFirmController.php 
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface; 
use App\Entity\MyfirmFirmdata;
use App\WikS\MyFirmUtils;
use App\WikS\BreadcrumbsUtil;
use App\WikS\ValidationFirm;

use Psr\Log\LoggerInterface;


/** wszystkie widoki związane z klientem
 * 
 */
class CustomersController extends AbstractController
{
    
    /** główny widok - lista klientów
     * @Route("/c", name="customers_main")
     */    
    public function customers_main(Request $request, PaginatorInterface $paginator, LoggerInterface $logger)
    {
        
        $bcu = new BreadcrumbsUtil();        
        $web_context = ['breadcrumbs'=> $bcu->customers()];
        
        $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class); 
        // pobieram wszystkich klientów, aby wyświetlić jako listę:
        $customers_objs = $paginator->paginate($repository->getAllCustomers(),$request->query->getInt('page', 1), 10);
        $web_context['customers_objs'] = $customers_objs;
        $logger->debug( 'znaleziono klientów: '.count($customers_objs) );
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoices_main');
            }
        }
        $contents = $this->renderView('customers/customers_main.html.twig', $web_context);
        return new Response($contents);
    }
    
    /** widok - konkretny klient
     * @Route("/c/s/{customer_id}", name="customer_show")
     */    
    public function customer_show(Request $request, LoggerInterface $logger, $customer_id)
    {
        
        $bcu = new BreadcrumbsUtil();
        $myfirm = new MyFirmUtils();
        $web_context = ['breadcrumbs'=> $bcu->customer_one()];
        
        $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class);
        // pobieram jednego klienta, aby go wyświetlić:
        $customer_obj = null;
        if(is_numeric($customer_id) && $customer_id > 1){
            $customer_obj = $repository->find($customer_id);  
        }
        // jeśli brak klienta --> przejdź do listy klientów
        if(!$customer_obj){
            $logger->debug( 'NIE znaleziono klientaID: '.$customer_id);
            return $this->redirectToRoute('customers_main');
        }
        
        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                return $this->redirectToRoute('invoices_main');
            }
            if($action == 'create_edit') {
                $logger->debug('edycja');
                return $this->redirectToRoute('customer_add_edit', ['customer_id' => $customer_id]);
            }
        }
        
        $web_context['firm_data_obj'] = $customer_obj;
        $contents = $this->renderView('customers/customer_show.html.twig', 
                                      $myfirm->update_webkontext($web_context, $customer_obj)
                                      );
        return new Response($contents);
    }
 
    /** widok - konkretny klient edycja
     * @Route("/c/e/{customer_id}", name="customer_add_edit")
     */    
    public function customer_add_edit(Request $request, LoggerInterface $logger, $customer_id=null)
    {
        
        $bcu = new BreadcrumbsUtil();
        $myfirm = new MyFirmUtils();
        $web_context = ['breadcrumbs'=> $bcu->customer_one()];

        // pobieram jednego klienta, aby go wyświetlić:
        $repository = $this->getDoctrine()->getRepository(MyfirmFirmdata::class);
        $customer_obj = null;
        if(is_numeric($customer_id) && $customer_id > 1){
            $customer_obj = $repository->find($customer_id);  
        }
        $em = $this->getDoctrine()->getManager();
        if(!$customer_obj){
            $logger->debug( 'NIE znaleziono klienta ');
            $customer_obj = new MyfirmFirmdata();
        }
        $web_context['firm_data_obj'] = $customer_obj;
        // z obiektu na stronę:
        $web_context = $myfirm->update_webkontext($web_context, $customer_obj); 
        
        $errors_message_list = [];
        $errors_message_redclass_list = [];

        if($request->getMethod() == 'POST') {
            $action = $request->request->get('action', null);
            $logger->debug('POST action... --> ' . $action);
            if($action == 'Cancel') {
                $logger->debug('Cancel');
                if($customer_id){
                    // gdy jest klient to do pokazania klienta
                    return $this->redirectToRoute('customer_show', ['customer_id' => $customer_id]);
                }else{
                    // jeśli nie ma takiego klienta to do listy klientów
                    return $this->redirectToRoute('customers_main');
                }
            }
            if($action == 'OK') {
                $logger->debug('zapis po sprawdzeniu danych z formularza do klienta');
                $vf = new ValidationFirm();
                list($firmName, 
                     $address_list, 
                     $mInputEmail, 
                     $firmNip, 
                     $firmRegon, 
                     $firmPesel, 
                     $firm_account_list) = $myfirm->data_firm_from_postrequest($request);
                
                //uzupełniam kontext, gdyby nie pyknęło...
                $web_context = $myfirm->from_request_to_webcontext_firm(
                                                    $web_context,
                                                    $firmName,
                                                    $address_list,
                                                    $mInputEmail,
                                                    $firmNip,
                                                    $firmRegon,
                                                    $firmPesel,
                                                    $firm_account_list);
                
                list($fname, 
                     $address_json, 
                     $email,
                     $nip, 
                     $regon, 
                     $pesel, 
                     $bank_json, 
                     $errors_message_list, 
                     $errors_message_redclass_list) = $vf->validate_complet_firm_data(
                                            $firmName,
                                            $address_list[0], $address_list[1], $address_list[2], $address_list[3], $address_list[4],
                                            $mInputEmail,
                                            $firmNip,
                                            $firmRegon,
                                            $firmPesel,
                                            $firm_account_list[0], $firm_account_list[1]);
                
                if(!$errors_message_redclass_list) {
                    $customer_obj->setFname($fname);
                    $customer_obj->setAddressJson($address_json);
                    $customer_obj->setEmail($email);
                    $customer_obj->setNip($nip);
                    $customer_obj->setRegon($regon);
                    $customer_obj->setPesel($pesel);
                    $customer_obj->setBankAccount($bank_json);
                    $customer_obj->setDtUpdate(new \DateTime);
                    $em->persist($customer_obj);
                    $em->flush();
                    return $this->redirectToRoute('customer_show', ['customer_id' => $customer_obj->getId()]);
                }
            }
        }
        
        $web_context['errors_message_list'] = $errors_message_list;
        $web_context['errors_message_redclass_list'] = $errors_message_redclass_list;
        
        $contents = $this->renderView('customers/customer_edit.html.twig', $web_context);
        return new Response($contents);
    }

}


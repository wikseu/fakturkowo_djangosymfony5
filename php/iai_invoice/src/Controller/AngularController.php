<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Controller/AngularController.php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\WikS\LookforCustomersUtils;
use App\WikS\LookforItemsUtils;
use Psr\Log\LoggerInterface;


/** odbiera żądania i wysyła dotyczące wyszukiwania firmy jako odbiorcy faktury
 *                                     oraz wyszukiwania towaru do faktury po nazwie
 * Description of AngularController
 *
 * @author wiks
 */
class AngularController extends AbstractController
{
        
    /**
     * @Route("/c/an", name="angular_url")
     */
    public function angular_url(Request $request, LoggerInterface $logger)
    {
        
        $result = [];        
        $data = json_decode($request->getContent(), true);
        if (json_last_error() === JSON_ERROR_NONE && isset($data["category"]) && isset($data["lookfor"])) {
            $logger->debug('rx: '. json_encode($data) ); //  rx: {"lookfor":"71055","category":"customer"}
            $em = $this->getDoctrine()->getManager();
            if($data["category"] == "customer") {
                $logger->debug('pytanie o klienta');
                $lookfor_customer_utils = new LookforCustomersUtils($logger, $em);
                $result = $lookfor_customer_utils->look_customer_nip_regon_pesel($data["lookfor"]);
            }
            elseif($data["category"] == "item") { 
                $logger->debug('pytanie o towar');
                $lookfor_items_utils = new LookforItemsUtils($logger, $em);
                $result = $lookfor_items_utils->look_items_name($data["lookfor"]);
            }
            $logger->debug('tx: '. json_encode($result) ); 
        }        
        return new JsonResponse($result);
    }
    
}

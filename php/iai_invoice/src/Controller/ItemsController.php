<?php
// src/Controller/MyFirmController.php 
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface; 
use App\Entity\ItemsItems;
use App\WikS\BreadcrumbsUtil;

use Psr\Log\LoggerInterface;

/** wszystkie widoki towarów, które można dodać do faktury
 * 
 */
class ItemsController extends AbstractController
{
    
    /** widok listy towarów
     * @Route("/t", name="items_list")
     */    
    public function items_list(Request $request, PaginatorInterface $paginator, BreadcrumbsUtil $bcu)
    {
        
        $repository = $this->getDoctrine()->getRepository(ItemsItems::class); 
        $contents = $this->renderView('items/items_list.html.twig', [ 
            'breadcrumbs'=> $bcu->items(),
            'items_list' => $paginator->paginate($repository->findAll(),
                                                 $request->query->getInt('page', 1), 10)
        ]);
        return new Response($contents);
    }
    
    // todo dodawanie i edycja towarów
    
    // todo klasa z rodzajem jednostek sztuki/kg etc
    
    // todo edycja klasy VAT
    
}


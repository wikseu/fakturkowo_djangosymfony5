<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\WikS;

use Doctrine\ORM\EntityManager;
use App\Entity\InvoicesInvoices;

/** validacja danych faktury
 * 
 * Description of ValidationInvoiceUtils
 *
 * @author wiks
 */
class ValidationInvoiceUtils {
    
    
    /** sprawdź, czy istnieje taka nazwa faktury jeszcze gdzieś oprócz tej
     * 
     * @param type $fNr
     * @param type $fid
     * @param type $entityManager
     * @return type
     */
    public function fvat_nr($fNr, $fid, $entityManager) {
        
        $fv_fNr = $fNr;
        $errors_message = 'proszę nadać numer fakturze';
        if($fNr) {
            $repository = $entityManager->getRepository(InvoicesInvoices::class);
            $is_used_fnr = $repository->isFNRused($fNr, $fid);
            if($is_used_fnr) {
                $errors_message = 'istnieje już faktura o takim numerze, podaj inny';
            }else{
                $errors_message = null;
                $fv_fNr = $fNr;
            }
        }
        return [$fv_fNr,
                $errors_message];
    }
    
    /**
     * 
     * @param type $invoice_obj
     * @param type $fNr
     * @param type $person_auth_name
     * @param type $dt_created
     * @param type $dt_pait_to
     * @param type $dt_delivery
     */
    public function validate_complet_invoice_data(EntityManager $entityManager,
                                                  $invoice_obj,
                                                  ...$args) {
        
        $errors_message_list = [];
        $errors_message_redclass_list = [];

        list($fv_fNr, 
             $errors_message) = $this->fvat_nr($args[0], $invoice_obj, $entityManager);
        if($errors_message){
            $errors_message_list[] = $errors_message;
            $errors_message_redclass_list[] = 'fNr';
        }
        $fv_person_auth_name = $args[1];
        $fv_dt_created = $args[2];
        $fv_dt_pait_to = $args[3];
        $fv_dt_delivery = $args[4];
        
        return [$fv_fNr, 
                $fv_person_auth_name, 
                $fv_dt_created, 
                $fv_dt_pait_to, 
                $fv_dt_delivery, 
                $errors_message_list, 
                $errors_message_redclass_list];
    }
}

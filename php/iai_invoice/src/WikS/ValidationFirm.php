<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\WikS;

/** validacja danych firmy
 * 
 * Description of ValidationFirm
 *
 * @author wiks
 */
class ValidationFirm {
    
    /** valisuj komplet, geeruj listę opisów błędów i listę klas do podświetlenia na web
     * 
     * @param type $args
     */
    public function validate_complet_firm_data(...$args) {
        
        $errors_message_list = [];
        $errors_message_redclass_list = [];

        list($fname, 
             $errors_message) = $this->fname($args[0]);
        if ($errors_message) {
            $errors_message_list[] = $errors_message;
            $errors_message_redclass_list[] = 'firmName';
        }
        list($address_json, 
             $errors_message) = $this->address($args[1], $args[2], $args[3], $args[4], $args[5]);
        if ($errors_message) {
            $errors_message_list[] = $errors_message;
            $errors_message_redclass_list[] = 'firmAdres';
        }
        $email = null;
        if ($args[6]) {
            list($email, 
                 $errors_message) = $this->vemail($args[6]);
            if ($errors_message) {
                $errors_message_list[] = $errors_message;
                $errors_message_redclass_list[] = 'mInputEmail';
            }
        }
        $nip = null;
        if ($args[7]) {
            list($nip, 
                 $errors_message) = $this->nip($args[7]);
            if ($errors_message) {
                $errors_message_list[] = $errors_message;
                $errors_message_redclass_list[] = 'firmNip';
            }
        }
        $regon = null;
        if ($args[8]) {
            list($regon, 
                 $errors_message) = $this->regon($args[8]);
            if ($errors_message) {
                $errors_message_list[] = $errors_message;
                $errors_message_redclass_list[] = 'firmRegon';
            }
        }
        $pesel = null;
        if ($args[9]) {
            list($pesel, 
                 $errors_message) = $this->pesel($args[9]);
            if ($errors_message) {
                $errors_message_list[] = $errors_message;
                $errors_message_redclass_list[] = 'firmPesel';
            }
        }
        if (!$nip && !$regon && !$pesel) {
            $errors_message = 'Wymagany jest jeden z: NIP, REGON, PESEL';
            $errors_message_list[] = $errors_message;
        }
        list($bank_json, 
             $errors_message) = $this->bank($args[10], $args[11]);
        if ($errors_message) {
            $errors_message_list[] = $errors_message;
            $errors_message_redclass_list[] = 'firmAccount';
        }
        return [$fname, 
                $address_json, 
                $email,
                $nip, 
                $regon, 
                $pesel, 
                $bank_json, 
                $errors_message_list, 
                $errors_message_redclass_list];        
    }
    
    /** sprawdza, czy jest nazwa firmy
     * 
     * @param type $fname
     * @return type
     */
    public function fname($fname) {
        
        $ret = $fname;
        $errors_message = null;
        if(!$fname){
            $errors_message = 'Przydałaby się nazwa firmy';
        }
        return [$ret, 
                $errors_message];
    }
    
    /** sprawdza (czy jest) adres i zamienia go w JSON
     * 
     * @param type $args
     */
    public function address(...$args) {
        
        $errors_message = 'Proszę wprowadzić choć jedną linię adresu';
        $address_list = [];
        foreach ($args as $arg) {
            $arg = trim($arg);
            if($arg && $arg != '') {
                $errors_message = null;
                $address_list[] = $arg;
            }
        }
        $address_json = json_encode($address_list);
        return [$address_json, 
                $errors_message];
    }
    
    /**
     * 
     * @param type $email
     */
    public function vemail($email) {
        
        $ret = $email;
        $errors_message = 'wprowadzony EMAIL nie jest poprawny';
        if (preg_match("/^[\w\.\+\-]+\@[\w\-]+\.[a-z]{2,3}$/", $email)) {
            $errors_message = null;
        }
        return [$ret, 
                $errors_message];
    }
    
    /** sprawdź nip
     * 
     * @param type $nip
     * @return type
     */
    public function nip($nip) {
        
        //substr("abcdef", -3, 1); // returns 'd'
        //
        // https://blog.aleksander.kaweczynski.pl/walidacja-numerow-pesel-nip-regon-w-javascript-i-php/

        $ret = $nip;
        $errors_message = 'wprowadzony NIP nie jest poprawny';
        $digits = trim($nip);
        $digits = str_replace('-', '', $digits);
        if(preg_match('/^\d{10}$/', $digits)) {
            //logger.debug('NIP sprawdzam... %s', list(digits) )
            $checksum = 0;
            $ct = [6, 5, 7, 2, 3, 4, 5, 6, 7];
            for($i=0;$i < count($ct); $i++) {
                $checksum += (intval($digits[$i]) * $ct[$i]);
            }
            $checksum = $checksum % 11;
            # logger.debug('checksum: %s', checksum)
            if (intval($digits[count($ct)]) == $checksum) {
                $ret = $digits;
                //logger.debug('NIP poprawny')
                $errors_message = null;
            }
        }
        return [$ret, 
                $errors_message];
    }
    
    /**
     * 
     * @param type $regon
     * @return type
     */
    public function regon($regon) {
        
        $ret = $regon;
        $ct = null;
        $errors_message = 'wprowadzony REGON nie jest poprawny';
        if ( preg_match('/^\d{9}$/', $regon) ) {
            $regon = trim($regon);
            //logger.debug('REGON9 sprawdzam... %s', list(regon))
            $ct = [8, 9, 2, 3, 4, 5, 6, 7];
        }
        if ( preg_match('/^\d{14}$/', $regon) ) {
            $regon = trim($regon);
            //logger.debug('REGON14 sprawdzam... %s', list(regon))
            $ct = [2, 4, 8, 5, 0, 9, 7, 3, 6, 1, 2, 4, 8];
        }
        if ($ct) {
            $checksum = 0;
            for ($i=0; $i< count($ct); $i++){
                $checksum += (intval($regon[$i]) * $ct[$i]);
            }
            $checksum = $checksum % 11;
            # logger.debug('checksum: %s', checksum)
            if (intval($regon[count($ct)]) == $checksum ) {
                //logger.debug('REGON poprawny')
                $errors_message = null;
            }
        }
        return [$ret, 
                $errors_message];
    }

    
    /**
     * 
     * @param type $pesel
     * @return type
     */
    public function pesel($pesel) {
        
        $ret = $pesel;
        $errors_message = 'wprowadzony PESEL nie jest poprawny';
        if ( preg_match('/^\d{11}$/', $pesel) ) {
//            logger.debug('PESEL sprawdzam...');
            $pesel = trim($pesel);
            $checksum = 0;
            $ct = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1];
            for($i=0;$i < count($ct); $i++){
                $checksum += (intval($pesel[$i]) * $ct[$i]);
            }
            if ($checksum % 10 == 0) {
                $ret = $pesel;
//                logger.debug('PESEL poprawny');
                $errors_message = null;
            }
        }
        return [$ret, 
                $errors_message];
    }
    
    /**
     * 
     * @param type $args
     * @return type
     */
    public function bank(...$args) {

//        75 1020 4870 0000 5602 0076 5859
//
//        https://pl.wikipedia.org/wiki/Numer_rachunku_bankowego
//        CC AAAA AAAA BBBB BBBB BBBB BBBB
//        http://phpedia.pl/wiki/Walidacja_numeru_NRB

        $bank_list = [];
        $errors_message = 'wprowadzony numer konta bankowego nie jest poprawny';
        foreach ($args as $arg) {
            if ($arg){
                $bank_list[] = trim($arg);
            }
        }
        if ($bank_list) {
            # logger.debug('bank account number validation: %s', bank_list[0])
            if ( preg_match('/^\d{2} \d{4} \d{4} \d{4} \d{4} \d{4} \d{4}$/', $bank_list[0]) 
                    || preg_match('/^\d{26}$/', $bank_list[0]) ) {
//                logger.debug('NR KONTA sprawdzam...');
                $account_number26 = str_replace(' ', '', $bank_list[0]);
                # logger.debug('bank account number validation: %s', account_number26)
                $ct = [1, 10,
                       3, 30, 9, 90,
                       27, 76, 81, 34,
                       49, 5, 50, 15,
                       53, 45, 62, 38,
                       89, 17, 73, 51,
                       25, 56, 75, 71,
                       31, 19, 93, 57];
                # $iNRB = substr($iNRB, 2).substr($iNRB, 0, 2);
                # od drugiego znaku . na końcu dwa ostatnie znaki
                //substr("abcdef", -3, 1); // returns 'd'
                $account_number26_reorgan = substr($account_number26, 2) . '2521' . substr($account_number26, 0, 2);
                # logger.debug('bank account number validation: %s', account_number26_reorgan)
                # DEBUG 2020-04-23 15:25:23,303 myvalidation bank account number validation:
                # 75102048700000560200765859
                # DEBUG 2020-04-23 15:25:23,303 myvalidation bank account number validation:
                # 102048700000560200765859252175
                $checksum = 0;
                for ($i=0; $i<count($ct); $i++){
                    $checksum += intval($account_number26_reorgan[count($ct) - 1 - $i]) * $ct[$i];
                }
                if ($checksum % 97 == 1){
//                    logger.debug('BANK poprawny');
                    $errors_message = null;
                }
            }
        }
        $bank_json = json_encode($bank_list);
        return [$bank_json, 
                $errors_message];
    }
    
}

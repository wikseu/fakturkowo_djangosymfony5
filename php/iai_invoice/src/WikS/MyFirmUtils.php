<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/WikS/MyFirmUtils.php 
namespace App\WikS;

/** narzędzia firmowe
 * 
 * Description of MyFirm
 * 
 * use App\WikS\MyFirmUtils;
 *
 * @author wiks
 */
class MyFirmUtils {
    
    /** pobranie danych firmy z formularza do zmiennych
     * 
     * @param type $request
     * @return type
     */
    public function data_firm_from_postrequest($request) {
        
        $address_list = ['', '', '', '', ''];
        $firm_account_list = ['', ''];

        $firmName = $request->request->get('firmName', '');
        $address_list[0] = $request->request->get('firmAdres1', '');
        $address_list[1] = $request->request->get('firmAdres2', '');
        $address_list[2] = $request->request->get('firmAdres3', '');
        $address_list[3] = $request->request->get('firmAdres4', '');
        $address_list[4] = $request->request->get('firmAdres5', '');
        $mInputEmail = $request->request->get('mInputEmail', '');
        $firmNip = $request->request->get('firmNip', '');
        $firmRegon = $request->request->get('firmRegon', '');
        $firmPesel = $request->request->get('firmPesel', '');
        $firm_account_list[0] = $request->request->get('firmAccount1', '');
        $firm_account_list[1] = $request->request->get('firmAccount2', '');
        return [$firmName, 
                $address_list, 
                $mInputEmail, 
                $firmNip, 
                $firmRegon, 
                $firmPesel, 
                $firm_account_list];
    }
    
    /** wyciągnij i uzupełnij dane z obiektu dla kontextu web
     * 
     * @param type $firm_data_obj
     */
    public function myfirm_pickup($firm_data_obj)
    {
        
        $firmName = $firm_data_obj->getfname();
        try{
            $address_list = json_decode($firm_data_obj->getaddressJson(), true);
            if(!$address_list ) {
                $address_list = ['', '', '', '', ''];
            }else{
                $address_list = array_merge($address_list, ['', '', '', '', '']);
            }
        }
        catch (Exception $e){
            $address_list = ['', '', '', '', ''];
        };
        $address_list = array_slice($address_list, 0, 5);
        $mInputEmail = $firm_data_obj->getemail();
        $firmNip = $firm_data_obj->getnip();
        $firmRegon = $firm_data_obj->getregon();
        $firmPesel = $firm_data_obj->getpesel();
        try{
            $firm_account_list = json_decode($firm_data_obj->getbankAccount(), true);
            if(!$firm_account_list) {
                $firm_account_list = ['', ''];
            }else{
                $firm_account_list = array_merge($firm_account_list, ['', '']);
            }
        }
        catch (Exception $e){
            $firm_account_list = ['', ''];
        };
        $firm_account_list = array_slice($firm_account_list, 0, 2);

        if(!$firmName){
            $firmName = '';
        }
        if(!$mInputEmail){
            $mInputEmail = '';
        }
        if(!$firmNip){
            $firmNip = '';
        }
        if(!$firmRegon){
            $firmRegon = '';
        }
        if(!$firmPesel){
            $firmPesel = '' ;       
        }
        return [$firmName, 
                $address_list, 
                $mInputEmail, 
                $firmNip, 
                $firmRegon, 
                $firmPesel, 
                $firm_account_list];
    }
    
    /** dodaj dane firmy do kontextu web
     * 
     * @param type $web_context
     * @param type $firmName
     * @param type $address_list
     * @param type $mInputEmail
     * @param type $firmNip
     * @param type $firmRegon
     * @param type $firmPesel
     * @param type $firm_account_list
     */
    public function from_request_to_webcontext_firm($web_context,
                                                    $firmName,
                                                    $address_list,
                                                    $mInputEmail,
                                                    $firmNip,
                                                    $firmRegon,
                                                    $firmPesel,
                                                    $firm_account_list)
    {
        $web_context['firmName'] = $firmName;
        $web_context['firmAdres1'] = $address_list[0];
        $web_context['firmAdres2'] = $address_list[1];
        $web_context['firmAdres3'] = $address_list[2];
        $web_context['firmAdres4'] = $address_list[3];
        $web_context['firmAdres5'] = $address_list[4];
        $web_context['mInputEmail'] = $mInputEmail;
        $web_context['firmNip'] = $firmNip;
        $web_context['firmRegon'] = $firmRegon;
        $web_context['firmPesel'] = $firmPesel;
        $web_context['firmAccount1'] = $firm_account_list[0];
        $web_context['firmAccount2'] = $firm_account_list[1];
        return $web_context;
    }
    
    /** uaktualni kontext danymi z obiektu
     * 
     * @param type $web_context
     * @param type $firm_data_obj
     * @return type
     */
    public function update_webkontext($web_context, $firm_data_obj)
    {
        
        list($firmName, 
             $address_list, 
             $mInputEmail, 
             $firmNip, 
             $firmRegon, 
             $firmPesel, 
             $firm_account_list) = $this->myfirm_pickup($firm_data_obj);
        
        return $this->from_request_to_webcontext_firm($web_context,
                                                      $firmName,
                                                      $address_list,
                                                      $mInputEmail,
                                                      $firmNip,
                                                      $firmRegon,
                                                      $firmPesel,
                                                      $firm_account_list);
    }
    
}

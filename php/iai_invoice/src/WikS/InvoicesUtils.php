<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use App\WikS\MyFirmUtils;

namespace App\WikS;


use Doctrine\ORM\EntityManager;
use App\Entity\InvoicesInvoices;
use App\Entity\InvoicesInvoicesitems; 
//use App\Entity\ItemsItems;
use App\Entity\ItemsVats;
use App\Entity\MyfirmFirmdata;
use App\WikS\DigitValues2words;

/**  narzędzia dla faktur
 * 
 * Description of InvoicesUtils
 *
 * @author wiks
 */
class InvoicesUtils {
    
    /** pobierz dane z formularza faktury
     * 
     * @param type $request
     * @return type
     */
    public function data_invoice_from_postrequest($request) {
        
        $fNr = $request->request->get('fNr', '');
        $paid_sum_brutto = $request->request->get('paid_sum_brutto', 0);
        $person_auth_name = $request->request->get('person_auth_name', '');
        $dt_created = $request->request->get('dt_created', '');
        $dt_pait_to = $request->request->get('dt_pait_to', '');
        $dt_delivery = $request->request->get('dt_delivery', '');
//        $action = $request->request->get('action', null);
        $pay_form_id = Null;  // todo przelew
        return [$fNr, 
                $person_auth_name, 
                $dt_created, 
                $dt_pait_to, 
                $dt_delivery, 
                $paid_sum_brutto, 
                $pay_form_id];
    }

    /** dane dodaj do kontextu dla widoku strony
     * 
     * @param type $web_context
     * @param type $fNr
     * @param type $customer
     * @param type $seller
     * @param type $person_auth_name
     * @param type $dt_created
     * @param type $dt_pait_to
     * @param type $dt_delivery
     * @param type $paid_sum_brutto
     * @return type
     */
    public function from_request_to_webcontext_invoice($web_context,
                                                       $fNr,
                                                       $customer,
                                                       $seller,
                                                       $person_auth_name,
                                                       $dt_created,
                                                       $dt_pait_to,
                                                       $dt_delivery,
                                                       $paid_sum_brutto) {
        
        $web_context['fNr'] = $fNr;
        $web_context['customer'] = $customer;
        $web_context['seller'] = $seller;
        # web_context['invoices_items_objs'] = invoices_items_objs
        # web_context['vat_sum'] = vat_sum
        $web_context['paid_sum_brutto'] = $paid_sum_brutto;
        # web_context['price_sum_brutto'] = price_sum_brutto
        # web_context['paid_form'] = paid_form
        $web_context['person_auth_name'] = $person_auth_name;
        $web_context['dt_created'] = $dt_created;
        $web_context['dt_pait_to'] = $dt_pait_to;
        $web_context['dt_delivery'] = $dt_delivery;
        return $web_context;
    }
    
    /** utwórz fakturę
     * 
     * @param EntityManager $entityManager
     * @param MyfirmFirmdata|null $customer
     * @return InvoicesInvoices
     */
    public function create_empty_invoice(EntityManager $entityManager, ?MyfirmFirmdata $customer)
    {
        
        $dt_now = new \DateTime();
        $dt_future = $dt_now;
        $dt_future->add(new \DateInterval('P30D'));
                        
        $invoice = new InvoicesInvoices();
        $invoice->setNr('');
        $invoice->setCustomer($customer);
        $invoice->setPriceSumBrutto(0);
        $invoice->setPriceSumNetto(0);
        $invoice->setPaidSumBrutto(0);
        $invoice->setPayFormId(null);
        $invoice->setDtCreated($dt_now);
        $invoice->setDtPaitTo($dt_future);
        $invoice->setDtDelivery($dt_future);
        $invoice->setPersonAuthName('');
        $invoice->setDtUpdate($dt_now);
        $entityManager->persist($invoice);
        $entityManager->flush();
        return $invoice;        
    }
    
    /** utwórz pusty słownik VAT dla podliczeń podatku
     * 
     * @return int
     */
    private function create_empty_vat_dict(EntityManager $entityManager) {
        
        $vat_tmp_dict = [];
        // do podliczenia VATu pobieram wszystkie jego stawki, dzięki temu będę miał też ułożone w kolejności
        $repository2 = $entityManager->getRepository(ItemsVats::class);
        $vat_objs = $repository2->findAll();
        foreach($vat_objs as $vat_one) {
            $vat_tmp_dict[$vat_one->getPercent()] = 0;
        }
        return $vat_tmp_dict;
    }
    
    /** oblicz jeden wiersz do ukazania faktury
     * 
     * @param type $invoices_item_obj
     * @return type
     */
    private function calculate_one_item_row_for_invoice($invoices_item_obj) {
        
        $vat = $invoices_item_obj->getItems()->getVat()->getPercent();            
        $price_netto = round($invoices_item_obj->getItems()->getPriceNetto(), 2);
        $price_brutto = round($price_netto * (100 + $vat) / 100, 2);
        $value_netto = round($price_netto * $invoices_item_obj->getItemsNumber(), 2);
        $value_brutto = round($value_netto * (100 + $vat) / 100, 2);
        $one_item_row_for_facture = [
            'id'=> $invoices_item_obj->getId(),
            'iid'=> $invoices_item_obj->getItems()->getId(),
            'name'=> $invoices_item_obj->getItems()->getName(),
            'items_number'=> $invoices_item_obj->getItemsNumber(),
            'jm'=> $invoices_item_obj->getItems()->getJm()->getName(),
            'price_netto'=> $price_netto,
            'price_brutto'=> $price_brutto,
            'value_netto' => $value_netto,
            'vat'=> $vat,
            'value_brutto'=> $value_brutto,
        ];
        return [$one_item_row_for_facture, $value_netto, $value_brutto, $vat];
    }
    
    /** przelicz VAT do tabeli finalnej
     * 
     * @param type $vat_tmp_dict
     * @return type
     */
    private function calculate_vat_final_table($vat_tmp_dict) {
        
        // teraz tworzę resztę tabeli podliczenia VAT:
        $vat_sum_list = [];
        $vat_sum_sum = ['wn'=> 0,
                        'wv'=> 0,
                        'wb'=> 0
                        ];
        
        foreach($vat_tmp_dict as $vat=> $sum_netto) {
            $sum_brutto = round($sum_netto * (100 + $vat) / 100, 2);
            $sum_netto = round($sum_netto, 2);
            $vat_sum_one_row = ['sv' => $vat,
                                'wn' => $sum_netto,
                                'wv' => round($sum_brutto - $sum_netto, 2),
                                'wb' => $sum_brutto,
                                ];
            $vat_sum_list[] = $vat_sum_one_row;
            $vat_sum_sum['wn'] += $vat_sum_one_row['wn'];
            $vat_sum_sum['wv'] += $vat_sum_one_row['wv'];
            $vat_sum_sum['wb'] += $vat_sum_one_row['wb'];
        }
        
        // podliczona tabela VAT:
        $vat_sum_sum['wn'] = round($vat_sum_sum['wn'], 2);
        $vat_sum_sum['wv'] = round($vat_sum_sum['wv'], 2);
        $vat_sum_sum['wb'] = round($vat_sum_sum['wb'], 2);       
        $vat_sum = [
            'list'=> $vat_sum_list,
            'sum'=> $vat_sum_sum
        ];
        return $vat_sum;
    }

    /** fakturę uzupełnij o obiekty towarów i zmień dla WEB
     * 
     * @param InvoicesInvoices|null $invoice_obj
     * @return type
     */
    public function invoice_pickup(EntityManager $entityManager, ?InvoicesInvoices $invoice_obj) {
        
        $fNr = $invoice_obj->getNr();
        $dt_created = $invoice_obj->getDtCreated();
        if ($dt_created) {
            $dt_created = $dt_created->format("Y-m-d");
        }else{
            $dt_created = '';
        }
        $dt_pait_to = $invoice_obj->getDtPaitTo();
        if ($dt_pait_to) {
            $dt_pait_to = $dt_pait_to->format("Y-m-d");
        }else{
            $dt_pait_to = '';
        }
        $dt_delivery = $invoice_obj->getDtDelivery();
        if ($dt_delivery) {
            $dt_delivery = $dt_delivery->format("Y-m-d");
        }else{
            $dt_delivery = '';
        }
        
        $customer = $invoice_obj->getCustomer();
        $repository0 = $entityManager->getRepository(MyfirmFirmdata::class);
        $seller = $repository0->find(1);
        
        $paid_form = 'przelew';
        // todo - dopracuj inne formy przelewy wg ID:
        if($invoice_obj->getPayFormId()) {
            $paid_form = 'gotówka';
        }

        $person_auth_name  = $invoice_obj->getPersonAuthName();
        $paid_sum_brutto = $invoice_obj->getPaidSumBrutto();
        $invoices_items_richer = [];  // tutaj utworzę postać dla faktury
        $price_sum_netto = 0;  // podliczę sumę netto $invoice_obj
        $price_sum_brutto = 0;  // podliczę sumę brutto
        
        // pobieram listę wszystkich dodanych do faktury produktów i ich liczbę
        $repository1 = $entityManager->getRepository(InvoicesInvoicesitems::class);
        $invoices_items_objs = $repository1->getAllItemsForInvoice($invoice_obj->getId());
        
        // jeśli chodzi o VAT, łatwiej będzie podliczyć w tymczasowej tabeli:
        $vat_tmp_dict = $this->create_empty_vat_dict($entityManager);

        // muszę podliczyć VATy a także utworzyć wpisy dla faktury
        foreach($invoices_items_objs as $invoices_item_obj) {
            
            list($one_item_row_for_facture, 
                 $value_netto, 
                 $value_brutto,
                 $vat) = $this->calculate_one_item_row_for_invoice($invoices_item_obj);
            $price_sum_netto += $value_netto;
            $price_sum_brutto += $value_brutto;
            $invoices_items_richer[] = $one_item_row_for_facture;
            // podliczam sumaryczny VAT:
            $vat_tmp_dict[$vat] += $value_netto;
        }
        
        $invoice_obj->setPriceSumNetto($price_sum_netto);
        $entityManager->flush();
        
        // teraz tworzę resztę tabeli podliczenia VAT:
        $vat_sum = $this->calculate_vat_final_table($vat_tmp_dict);
        return [$fNr,  
                $customer, 
                $seller, 
                $invoices_items_richer, 
                $vat_sum, 
                round($price_sum_netto, 2), 
                round($price_sum_brutto, 2), 
                $paid_form, 
                $person_auth_name, 
                $dt_created, 
                $dt_pait_to, 
                $dt_delivery, 
                $paid_sum_brutto];
    }
    
    /** dodaj przedmiot do faktury
     * 
     * @param EntityManager $entityManager
     * @param type $invoice_obj
     * @param type $item_obj
     * @param type $item_amount
     * @param type $errors_message_list
     */
    public function add_item_to_invoice(EntityManager $entityManager, $invoice_obj, $item_obj, $item_amount, $errors_message_list) {
        
        $invoices_item_obj = null;
        $item_amount_value = null;
        if(!$item_amount || trim($item_amount) == '') {
            $errors_message = 'proszę podać ilość w ' + str($item_obj->getJm()->getName());
            $errors_message_list[] = $errors_message;
        }else{
            try{
                $item_amount_value = floatval($item_amount);
            }
            catch (Exception $e){
                $errors_message = 'proszę spróbować jeszcze raz';
                $errors_message_list[] = $errors_message;
            }
            if($item_amount_value) {
                if($item_amount_value < 0) {
                    $errors_message = 'wymagana wartość dodatnia';
                    $errors_message_list[] = $errors_message;
                }else{
                    // jeśli jest w sztukach to nie może być ułamkiem...
                    $kind_of_measure = $item_obj->getJm()->getId();  // szt to ID=1
                    if($kind_of_measure == 1) {
                        // tutaj musi być wartością całkowitą, bo dotyczy sztuk
                        if($item_amount_value - intval($item_amount_value) != 0){
                            $errors_message = 'proszę spróbować jeszcze raz, wymagana ilość sztuk';
                            $errors_message_list[] = $errors_message;
                        }
                    }
                }
            }
        }
        if( !$errors_message_list && $item_amount_value) {
            // dodajemy przedmiot do faktury, 
            // sprawdzam wcześniej czy na tej fakturze istnieje ten przedmiot, 
            // jeśli tak to nadpisuję go
            $repository = $entityManager->getRepository(InvoicesInvoicesitems::class);
            $invoices_item_obj = $repository->getInvoiceItemIfExist(
                                                    $invoice_obj->getId(), 
                                                    $item_obj->getId());
            if(!$invoices_item_obj) {
                $invoices_item_obj = new InvoicesInvoicesitems();
                $invoices_item_obj->setInvoice($invoice_obj);
                $invoices_item_obj->setItems($item_obj);
                $entityManager->persist($invoices_item_obj);
                $entityManager->flush();
            }
            $invoices_item_obj->setItemsNumber($item_amount_value);
            $entityManager->flush();
        }
        return [$invoices_item_obj, 
                $errors_message_list];        
    }
    
    /** utwórz dane do widoku pokazania/edycji faktury - uniwesalne
     * 
     * @param EntityManager $entityManager
     * @param type $web_context
     * @param InvoicesInvoices|null $invoice_obj
     * @param type $logger
     * @return type
     */
    public function common_invoice_middle_show(EntityManager $entityManager, 
                                               $web_context, 
                                               ?InvoicesInvoices $invoice_obj) {
        $mfu = new MyFirmUtils();
        list($web_context['fNr'],
             $customer, 
             $seller, 
             $web_context['invoices_items_richer'], 
             $web_context['vat_sum'], 
             $web_context['price_sum_netto'], 
             $price_sum_brutto, 
             $web_context['paid_form'], 
             $web_context['person_auth_name'], 
             $web_context['dt_created'], 
             $web_context['dt_pait_to'], 
             $web_context['dt_delivery'], 
             $web_context['paid_sum_brutto']) = $this->invoice_pickup($entityManager, $invoice_obj);
        $web_context['price_sum_brutto'] = $price_sum_brutto;  // do tłumaczenia na słowną wartość

        $shortfirm_seller = [];
        list($shortfirm_seller['firmName'], 
             $shortfirm_seller['address_list'], 
             $shortfirm_seller['mInputEmail'], 
             $shortfirm_seller['firmNip'], 
             $shortfirm_seller['firmRegon'], 
             $shortfirm_seller['firmPesel'], 
             $shortfirm_seller['firm_account_list']) = $mfu->myfirm_pickup($seller);
        $web_context['shortfirm_seller'] = $shortfirm_seller;

        $shortfirm_customer = [];
        list($shortfirm_customer['firmName'], 
             $shortfirm_customer['address_list'], 
             $shortfirm_customer['mInputEmail'], 
             $shortfirm_customer['firmNip'], 
             $shortfirm_customer['firmRegon'], 
             $shortfirm_customer['firmPesel'], 
             $shortfirm_customer['firm_account_list']) = $mfu->myfirm_pickup($customer);
        $web_context['shortfirm_customer'] = $shortfirm_customer;
        
        // przetłumacz na słownie:        
        $v2w = new DigitValues2words();
        $web_context['price_sum_brutto_translated'] = $v2w->currency_translate($price_sum_brutto);
        return $web_context;
    }
    
}

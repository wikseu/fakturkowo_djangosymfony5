<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/WikS/DigitValues2words.php

namespace App\WikS;

/** zabawa z przepisaniem kwoty słownie na fakturze
 * Description of DigitValues2words
 *
 * @author wiks
 */
class DigitValues2words {
    
    private $units_list;
    private $dozens_list;
    private $hundreads_of;
    private $teens;
    private $bigones;
    
    public function __construct() {
        
        $this->units_list = ["zero", "jeden", "dwa", "trzy", "cztery", "pięć", "sześć",
                             "siedem", "osiem", "dziewięć"];
        $this->dozens_list = ["", "dziesiec", "dzwadziescia", "trzydziesci", "czterdziesci",
                              "pięćdziesiąt", "szcześćdziesiąt", "siedemdziesiąt", "osiemdziesiąt",
                              "dziewięćdziesiat"];
        $this->hundreads_of = ["", "sto", "dwieście", "trzysta", "czerysta", "pięćset",
                               "sześcset", "siedemset", "osiemset", "dziewięćset"];
        $this->teens = ["dziesięć", "jedenaście", "dwanaście", "trzynaście",
                        "czternaście", "piętnaście", "szesznaście", "siediemanście",
                        "osiemnaście", "dziewiętnaście"];
        $this->bigones = ["", "tysiąc", "milion", "miliard", "bilion", "biliard",
                          "trylion", "tryliard", "kwadrylion", "kwadryliard", "kwintylion"];
    }
    
    /**
     * 
     * @param type $list2convert
     * @return type
     */
    public function convert_it(string $list2convert) {
        

        $result = "";
        while ($list2convert[0] == '0') {
            $list2convert = substr($list2convert, 1);
            if( strlen($list2convert) == 0){
                return $result;
            }
        }
        if(strlen($list2convert) == 3){
            $result .= $this->hundreads_of[intval(substr($list2convert, -3, 1))]." ";  // substr("abcdef", -3, 1); // returns 'd'
            $list2convert = substr($list2convert, 1);  // list2convert[1:]
            while($list2convert && $list2convert[0] == '0'){
                $list2convert = substr($list2convert, 1);
            }
        }
        if (strlen($list2convert) == 2) {
            if( $list2convert[-2] == "1" ) {
                $result .= $this->teens[intval(substr($list2convert, -1, 1))]." ";  // list2convert[-1]
                return $result;
            }else{
                $result .= $this->dozens_list[intval(substr($list2convert, -2, 1))] . " ";
                if(intval(substr($list2convert, -1, 1)) > 0) {
                    $result .= $this->units_list[intval(substr($list2convert, -1, 1))] . " ";  // list2convert[-1]
                }
                return $result;
            }
        }
        if(strlen($list2convert) == 1){
            $result .= $this->units_list[intval($list2convert[0])];
            return $result;
        }
        return $result;
    }

    /**
     * 
     * @param type $mylist
     * @param type $digits_length
     * @return string
     */
    private function postscript($mylist, $digits_length) {
        
        if ($digits_length == 0) {
            return "";
        }
        $a = 0;
        $b = 0;
        $c = 0;
        if (strlen($mylist) == 1){
            $c = intval($mylist[0]);
        }
        if (strlen($mylist) == 2) {
            $b = intval($mylist[0]);
            $c = intval($mylist[1]);
        }
        if (strlen($mylist) == 3) {
            $a = intval($mylist[0]);
            $b = intval($mylist[1]);
            $c = intval($mylist[2]);
        }
        if($a == 0 && $b == 0 && $c == 1) {
            if($digits_length == 1){
                return "tysiąc ";
            }else{
                return $this->bigones[$digits_length] . " ";
            }
        }
        if($b == 1){
            if($digits_length == 1){
                return "tysięcy ";
            }else{
                return $this->bigones[$digits_length] . "ów ";
            }
        }else{
            if($c == 2 || $c == 3 || $c == 4){
                if($digits_length == 1) {
                    return "tysiące ";
                }else{
                    return $this->bigones[$digits_length] . "y ";
                }
            }else{
                if($digits_length == 1) {
                    return "tysięcy ";
                }else{
                    return $this->bigones[$digits_length] . "ów ";
                }
            }
        }
    }
    
    /**
     * 
     * @param type $digits_value
     * @return string
     */
    private function translate($digits_value) {
        
        $result = "";
        if ($digits_value == 0) {
            $result = "zero";
            return $result;
        }
        $digits_as_string = ''.($digits_value);
        $rest_of = strlen($digits_as_string) % 3;
        $digits_length = intval((strlen($digits_as_string) - 1) / 3);
        if( $rest_of == 1) {
            $result .= $this->convert_it($digits_as_string[0]) . ' ';
            $result .= $this->postscript('0' . '0' . $digits_as_string[0], $digits_length);
            $digits_length -= 1;
            $digits_as_string = substr($digits_as_string, 1);  // $digits_as_string[1:];
        }
        if( $rest_of == 2 ) {
            $result .= $this->convert_it($digits_as_string[0] . $digits_as_string[1]);
            $result .= $this->postscript('0' . $digits_as_string[0] . $digits_as_string[1], $digits_length);
            $digits_length -= 1;
            $digits_as_string = substr($digits_as_string, 2);  //  digits_as_string[2:];
        }
        for($i=0; $i< strlen($digits_as_string); $i=$i+3){
            $result .= $this->convert_it($digits_as_string[$i] . $digits_as_string[$i + 1] . $digits_as_string[$i + 2]);
            if( $digits_as_string[$i] != '0' || $digits_as_string[$i + 1] != '0' || $digits_as_string[$i + 2] != '0') {
                $result .= $this->postscript($digits_as_string[$i] . $digits_as_string[$i + 1] . $digits_as_string[$i + 2], $digits_length);
            }
            $digits_length -= 1;
        }
        return $result;
    }
    
    /**
     * 
     * @param type $price
     * @return type
     */
    public function currency_translate($price) {
        
        $integ = intval($price);
        $price_rest = intval(($price - $integ) * 100);
        return $this->translate($integ) . " " . $price_rest . "/100";
    }
                
}

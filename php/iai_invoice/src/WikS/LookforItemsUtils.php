<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\WikS;

use Doctrine\ORM\EntityManager;
use App\Entity\ItemsItems;
use Psr\Log\LoggerInterface;

/**
 * Description of LookforUtils
 * LookforUtils
 * @author wiks
 */
class LookforItemsUtils 
{
    
    private $entityManager;

    /**
     * 
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     */
    public function __construct(LoggerInterface $logger, EntityManager $entityManager) {
        
        $this->logger = $logger;
        $this->entityManager = $entityManager; 
    }
    
    /** wyszukaj towary po fragmencie nazwy
     * 
     * @param type $lookfor
     */
    public function look_items_name(string $lookfor) { 
        
        $item_objs = null;
        $item_list = [];
        if(strlen($lookfor) >= 3){
            $em = $this->entityManager;        
            $repository = $em->getRepository(ItemsItems::class);
            $item_objs = $repository->lookforItemLike($lookfor);
            $this->logger->debug('ITEMS MATCH COUNT:'. count($item_objs) );
            for($i=0;$i<count($item_objs);$i++) {
                $item_list[] = [$item_objs[$i]['id'], $item_objs[$i]['name']];
            }
        }
        return $item_list;
    }
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\WikS;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use App\Entity\MyfirmFirmdata;

/**
 * Description of LookforCustomersUtils
 * LookforUtils
 * @author wiks
 */
class LookforCustomersUtils 
{
    
    private $entityManager;

    /**
     * 
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     */
    public function __construct(LoggerInterface $logger, EntityManager $entityManager) {
        
        $this->logger = $logger;
        $this->entityManager = $entityManager; 
    }

    /** wyszukaj klientów po numerze zawartym w NIP, REGON lub PESEL
     * 
     * @param type $lookfor
     */
    public function look_customer_nip_regon_pesel(string $lookfor) {
        
        $firm_data_name_objs = null;
        if(strlen($lookfor) >= 3){
        
            $em = $this->entityManager;        
            $repository = $em->getRepository(MyfirmFirmdata::class);
            $firm_data_name_objs = $repository->lookforNipRegonPeselLike($lookfor);
            $this->logger->debug('CUSTOMERS MATCH COUNT:'. count($firm_data_name_objs) );
        }
        return $firm_data_name_objs;
    }
    
}

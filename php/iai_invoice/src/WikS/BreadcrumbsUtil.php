<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// src/WikS/BreadcrumbsUtil.php
namespace App\WikS;

/** zabazy z tworzniem okruszków adresu URL i wskazanie miejsca na stronie
 * Description of BreadcrumbsUtil
 * 
 * use App\WikS\BreadcrumbsUtil;
 * , BreadcrumbsUtil $bcu
 * 'breadcrumbs'=> $bcu->items(),
 *
 * @author wiks
 */
class BreadcrumbsUtil {
    
    /** dodaj okruszek
     * 
     * @param type $name
     * @param type $url
     */
    private function add_one($name, $url)
    {
        
        array_push($this->breadcrumbs, 
                ['name'=> $name, 'url'=> $url]);
    }
    
    /** utwórz dla INDEX
     * 
     * @return type
     */
    public function home(){
        
        $this->breadcrumbs = [];
        $this->add_one('Task', 'index');
        $this->add_one('Home', 'invoices_main');
        return $this->breadcrumbs;
    }
    
    /** utwórz dla mojej firmy
     * 
     * @return type
     */
    public function myfirm(){
        
        $this->home();
        $this->add_one('moja firma', 'myfirm_main');
        return $this->breadcrumbs;
    }

    /** utwórz dla listy klientów
     *  
     * @return type
     */
    public function customers(){
        
        $this->home();
        $this->add_one('klienci', 'customers_main');
        return $this->breadcrumbs;
    }

    /** utwórz dla konkretnego klienta (można wskazań jego nazwę)
     * 
     * @param type $klient
     * @return type
     */
    public function customer_one(string $klient='klient'){
        
        $this->home();
        $this->add_one($klient, null);
        return $this->breadcrumbs;
    }

    /** utwórz dla listy faktur
     * 
     * @return type
     */
    public function invoices(){
        
        $this->home();
        $this->add_one('faktury', 'invoices_list');
        return $this->breadcrumbs;
    }

    /** utwórz dla konkretnej faktury (można wskazać jej nazwę)
     * 
     * @param type $invoice
     * @return type
     */
    public function invoice_one(string $invoice='faktura'){
        
        $this->invoices();
        $this->add_one($invoice, null);
        return $this->breadcrumbs;
    }

    /** utwórz dla kisty towarów
     * 
     * @return type
     */
    public function items(){
        
        $this->home();
        $this->add_one('towary', 'items_list');
        return $this->breadcrumbs;
    }
    
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemsVats
 *
 * @ORM\Table(name="items_vats")
 * @ORM\Entity
 */
class ItemsVats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="percent", type="integer", nullable=false)
     */
    private $percent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPercent(): ?int
    {
        return $this->percent;
    }

    public function setPercent(int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MyfirmFirmdata
 *
 * @ORM\Table(name="myfirm_firmdata")
 * @ORM\Entity(repositoryClass="App\Repository\MyfirmFirmdataRepository")
 */
class MyfirmFirmdata
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="text", length=0, nullable=false)
     */
    private $fname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address_json", type="text", length=0, nullable=true)
     */
    private $addressJson;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nip", type="text", length=0, nullable=true)
     */
    private $nip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="regon", type="text", length=0, nullable=true)
     */
    private $regon;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pesel", type="text", length=0, nullable=true)
     */
    private $pesel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bank_account", type="text", length=0, nullable=true)
     */
    private $bankAccount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="text", length=0, nullable=true)
     */
    private $icon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_update", type="datetime", nullable=false)
     */
    private $dtUpdate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getFname(): ?string
    {
        return $this->fname;
    }

    public function setFname(string $fname): self
    {
        $this->fname = $fname;

        return $this;
    }

    public function getAddressJson(): ?string
    {
        return $this->addressJson;
    }

    public function setAddressJson(?string $addressJson): self
    {
        $this->addressJson = $addressJson;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNip(): ?string
    {
        return $this->nip;
    }

    public function setNip(?string $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getRegon(): ?string
    {
        return $this->regon;
    }

    public function setRegon(?string $regon): self
    {
        $this->regon = $regon;

        return $this;
    }

    public function getPesel(): ?string
    {
        return $this->pesel;
    }

    public function setPesel(?string $pesel): self
    {
        $this->pesel = $pesel;

        return $this;
    }

    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    public function setBankAccount(?string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getDtUpdate(): ?\DateTimeInterface
    {
        return $this->dtUpdate;
    }

    public function setDtUpdate(\DateTimeInterface $dtUpdate): self
    {
        $this->dtUpdate = $dtUpdate;

        return $this;
    }


}

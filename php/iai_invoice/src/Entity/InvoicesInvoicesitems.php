<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoicesInvoicesitems
 *
 * @ORM\Table(name="invoices_invoicesitems", indexes={@ORM\Index(name="invoices_invoicesitems_items_id_51634ca0_fk_items_items_id", columns={"items_id"}), @ORM\Index(name="invoices_invoicesite_invoice_id_55c98b5f_fk_invoices_", columns={"invoice_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\InvoicesInvoicesitemsRepository")
 */
class InvoicesInvoicesitems
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float|null
     *
     * @ORM\Column(name="items_number", type="float", precision=10, scale=0, nullable=true)
     */
    private $itemsNumber;

    /**
     * @var \InvoicesInvoices
     *
     * @ORM\ManyToOne(targetEntity="InvoicesInvoices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;

    /**
     * @var \ItemsItems
     *
     * @ORM\ManyToOne(targetEntity="ItemsItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="items_id", referencedColumnName="id")
     * })
     */
    private $items;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemsNumber(): ?float
    {
        return $this->itemsNumber;
    }

    public function setItemsNumber(?float $itemsNumber): self
    {
        $this->itemsNumber = $itemsNumber;

        return $this;
    }

    public function getInvoice(): ?InvoicesInvoices
    {
        return $this->invoice;
    }

    public function setInvoice(?InvoicesInvoices $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getItems(): ?ItemsItems
    {
        return $this->items;
    }

    public function setItems(?ItemsItems $items): self
    {
        $this->items = $items;

        return $this;
    }


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoicesInvoices
 *
 * @ORM\Table(name="invoices_invoices", indexes={@ORM\Index(name="invoices_invoices_customer_id_143e6705_fk_myfirm_firmdata_id", columns={"customer_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\InvoicesInvoicesRepository")
 */
class InvoicesInvoices
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nr", type="string", length=50, nullable=true)
     */
    private $nr;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt_created", type="date", nullable=true)
     */
    private $dtCreated;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt_delivery", type="date", nullable=true)
     */
    private $dtDelivery;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pay_form_id", type="integer", nullable=true)
     */
    private $payFormId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt_pait_to", type="date", nullable=true)
     */
    private $dtPaitTo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="person_auth_name", type="text", length=0, nullable=true)
     */
    private $personAuthName;

    /**
     * @var string
     *
     * @ORM\Column(name="price_sum_netto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $priceSumNetto;

    /**
     * @var string
     *
     * @ORM\Column(name="price_sum_brutto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $priceSumBrutto;

    /**
     * @var string
     *
     * @ORM\Column(name="paid_sum_brutto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $paidSumBrutto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_update", type="datetime", nullable=false)
     */
    private $dtUpdate;

    /**
     * @var \MyfirmFirmdata
     *
     * @ORM\ManyToOne(targetEntity="MyfirmFirmdata")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNr(): ?string
    {
        return $this->nr;
    }

    public function setNr(?string $nr): self
    {
        $this->nr = $nr;

        return $this;
    }

    public function getDtCreated(): ?\DateTimeInterface
    {
        return $this->dtCreated;
    }

    public function setDtCreated(?\DateTimeInterface $dtCreated): self
    {
        $this->dtCreated = $dtCreated;

        return $this;
    }

    public function getDtDelivery(): ?\DateTimeInterface
    {
        return $this->dtDelivery;
    }

    public function setDtDelivery(?\DateTimeInterface $dtDelivery): self
    {
        $this->dtDelivery = $dtDelivery;

        return $this;
    }

    public function getPayFormId(): ?int
    {
        return $this->payFormId;
    }

    public function setPayFormId(?int $payFormId): self
    {
        $this->payFormId = $payFormId;

        return $this;
    }

    public function getDtPaitTo(): ?\DateTimeInterface
    {
        return $this->dtPaitTo;
    }

    public function setDtPaitTo(?\DateTimeInterface $dtPaitTo): self
    {
        $this->dtPaitTo = $dtPaitTo;

        return $this;
    }

    public function getPersonAuthName(): ?string
    {
        return $this->personAuthName;
    }

    public function setPersonAuthName(?string $personAuthName): self
    {
        $this->personAuthName = $personAuthName;

        return $this;
    }

    public function getPriceSumNetto(): ?string
    {
        return $this->priceSumNetto;
    }

    public function setPriceSumNetto(string $priceSumNetto): self
    {
        $this->priceSumNetto = $priceSumNetto;

        return $this;
    }

    public function getPriceSumBrutto(): ?string
    {
        return $this->priceSumBrutto;
    }

    public function setPriceSumBrutto(string $priceSumBrutto): self
    {
        $this->priceSumBrutto = $priceSumBrutto;

        return $this;
    }

    public function getPaidSumBrutto(): ?string
    {
        return $this->paidSumBrutto;
    }

    public function setPaidSumBrutto(string $paidSumBrutto): self
    {
        $this->paidSumBrutto = $paidSumBrutto;

        return $this;
    }

    public function getDtUpdate(): ?\DateTimeInterface
    {
        return $this->dtUpdate;
    }

    public function setDtUpdate(\DateTimeInterface $dtUpdate): self
    {
        $this->dtUpdate = $dtUpdate;

        return $this;
    }

    public function getCustomer(): ?MyfirmFirmdata
    {
        return $this->customer;
    }

    public function setCustomer(?MyfirmFirmdata $customer): self
    {
        $this->customer = $customer;

        return $this;
    }


}

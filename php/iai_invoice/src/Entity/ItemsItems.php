<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemsItems
 *
 * @ORM\Table(name="items_items", indexes={@ORM\Index(name="items_items_vat_id_3db0214e_fk_items_vats_id", columns={"vat_id"}), @ORM\Index(name="items_items_jm_id_864d4159_fk_items_jms_id", columns={"jm_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ItemsItemsRepository")
 */
class ItemsItems
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=0, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price_netto", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $priceNetto;

    /**
     * @var \ItemsJms
     *
     * @ORM\ManyToOne(targetEntity="ItemsJms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="jm_id", referencedColumnName="id")
     * })
     */
    private $jm;

    /**
     * @var \ItemsVats
     *
     * @ORM\ManyToOne(targetEntity="ItemsVats")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vat_id", referencedColumnName="id")
     * })
     */
    private $vat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriceNetto(): ?string
    {
        return $this->priceNetto;
    }

    public function setPriceNetto(string $priceNetto): self
    {
        $this->priceNetto = $priceNetto;

        return $this;
    }

    public function getJm(): ?ItemsJms
    {
        return $this->jm;
    }

    public function setJm(?ItemsJms $jm): self
    {
        $this->jm = $jm;

        return $this;
    }

    public function getVat(): ?ItemsVats
    {
        return $this->vat;
    }

    public function setVat(?ItemsVats $vat): self
    {
        $this->vat = $vat;

        return $this;
    }


}

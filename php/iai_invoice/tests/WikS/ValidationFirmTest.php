<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Tests\Util;

use App\WikS\ValidationFirm;
use PHPUnit\Framework\TestCase;


/**
 * Description of ValidationFirmTest
 *
 * @author wiks
 */
class ValidationFirmTest extends TestCase
{
    
    /**
     * 
     */
    public function testfname() {
        
        $vf = new ValidationFirm();
        $pairs = [['', ['', 'Przydałaby się nazwa firmy']],
            ['nazwenka', ['nazwenka', '']],
            ];
        foreach($pairs as $pair) {
            $result = $vf->fname($pair[0]);
            $this->assertEquals($pair[1], $result);
        }
    }
    
    
//    public function testaddress() {
//        
//        $vf = new ValidationFirm();
//        
//        $pairs = [ [[], ['', 'Przydałaby się nazwa firmy']],
//                   [['nazwenka'], ['nazwenka', '']],
//            ];
//        
//        foreach($pairs as $pair) {
//        
//            // todo tutaj trzeba dać wnętrze array a nie samą array ----------------------------
//            
//            $result = $vf->address($pair[0]);
//            $this->assertEquals($pair[1], $result);
//        }
//
//    }
    
    /**
     * 
     */
    public function testvemail() {

        $vf = new ValidationFirm();
        $pairs = [['wiks@wiks.eu', ['wiks@wiks.eu', null]],
            ['biuro@fn-service.pl', ['biuro@fn-service.pl', '']],
            ['nazwenka', ['nazwenka', 'wprowadzony EMAIL nie jest poprawny']],
            ];
        foreach($pairs as $pair) {
            $result = $vf->vemail($pair[0]);
            $this->assertEquals($pair[1], $result);
        }
    }

    
    /**
     * 
     */
    public function testnip() {

        $vf = new ValidationFirm();
        $pairs = [['573-214-78-63', ['5732147863', null]],
                  ['73-214-78-63', ['73-214-78-63', 'wprowadzony NIP nie jest poprawny']],

            ];
        foreach($pairs as $pair) {
            $result = $vf->nip($pair[0]);
            $this->assertEquals($pair[1], $result);
        }
    }

    /**
     * 
     */
    public function testregon() {

        $vf = new ValidationFirm();
        $pairs = [['573-214-78-63', ['573-214-78-63', 'wprowadzony REGON nie jest poprawny']],
                  ['123456785', ['123456785', null]],
                  ['12345678512347', ['12345678512347', null]],
            ];
        foreach($pairs as $pair) {
            $result = $vf->regon($pair[0]);
            $this->assertEquals($pair[1], $result);
        }
    }

    /**
     * 
     */
    public function testpesel() {

        $vf = new ValidationFirm();
        $pairs = [['573-214-78-63', ['573-214-78-63', 'wprowadzony PESEL nie jest poprawny']],
                  ['71050103357', ['71050103357', null]],
            ];
        foreach($pairs as $pair) {
            $result = $vf->pesel($pair[0]);
            $this->assertEquals($pair[1], $result);
        }
    }    
    
    /**
     * 
     */
    public function testbank() {

        $vf = new ValidationFirm();
        $pairs = [
                  ['75102048700000560200765859', ['["75102048700000560200765859"]', null]],
                  ['75 1020 4870 0000 5602 0076 5859', ['["75 1020 4870 0000 5602 0076 5859"]', null]],
                  ['75 1020 4870 0000 5602 0076 5858', ['["75 1020 4870 0000 5602 0076 5858"]', 'wprowadzony numer konta bankowego nie jest poprawny']],
            ];
        foreach($pairs as $pair) {
            $result = $vf->bank($pair[0]);
            $this->assertEquals($pair[1], $result);
        }
    }
    
    
}
